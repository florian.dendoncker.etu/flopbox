# FlopBox

**Etudiant** : DENDONCKER Florian



##### Lancer le programme : 

*Depuis le dossier source du programme* : 

```
mvn package
mvn exec:java
```



##### Informations de connexion au serveur FTP local : 

```
FTP Server Address : 0.0.0.0
FTP Server Port    : 2121
URI port           : 8080
Username           : anonymous
Password           : anonymous
```

Une erreur se declenche a la connexion, le programme se connecte au serveur FTP, mais se deconnecte immediatement avec cette erreur , je n'ai pas réussi a passer outre ce problème.

```exception occured while executing the Java class. null: InvocationTargetException: Failed to start Grizzly HTTP server: Address already in use``` 



##### Structure du projet : 

###### Package Flopbox : 

- **Main** : Contient la logique permettant de récupérer les informations utilisées pour se connecter au serveur FTP depuis les inputs utilisateurs, ainsi que la logique de mise en place de la connexion au FTP et la logique de boucle d'attente d'inputs utilisateur.
- **MyResource** : Contient toutes les differentes actions qu'il est possible d'effectuer sur le serveur FTP (remonter d'un dossier, uploder ou renommer un fichier,...) executant le code contenu dans les classes équivalentes du package **flopbox.commands**.
- **URIUtil** : Contient les outils necessaires a la verification de la validité de l'URI

###### Package Flopbox.Accounts : 

- **AccountsUtil** : Contient le code permettant de charger et de verifier le code contenu dans le fichier **accounts.txt** du même dossier. (sert a gerer les comptes utilisateurs)

###### Package Flopbox.Commands : 

- Contient une classe par action possible sur le serveur FTP (LIST, LOGIN, QUIT, ...), chaque classe contenant une fonction **execute** déclenchant son execution sur le serveur FTP depuis le client.

###### Package Flopbox.Server : 

- **AliasesUtil** : Permet de charger la liste des aliases contenus dans le fichier **aliases.txt**. (contient des aliases de noms de serveurs ftp)
- **FTPServerUtil** : Permet la mise en place et la verification de la validite de l'adresse et du port du serveur FTP.


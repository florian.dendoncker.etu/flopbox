package flopbox;

import flopbox.accounts.AccountsUtil;
import flopbox.server.*;
import org.apache.commons.net.ftp.FTPClient;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import java.io.IOException;
import java.net.URI;
import java.util.Scanner;

public class Main {
	private static String BASE_URI; // URI the Grizzly HTTP server will listen on
	private static FTPClient client;
	private static String serverAddress;
	private static int serverPort;
	private static String username;
	private static String password;


	public static String getBaseUri(){
		return BASE_URI;}

	public static FTPClient getClient(){
		return client;}

	public static String getServerAddress(){
		return serverAddress;}

	public static int getServerPort(){
		return serverPort;}

	public static String getUsername(){
		return username;}

	public static String getPassword(){
		return password;}
	

	public static void setServerPort(int newPort){
		serverPort = FtpServerUtil.setFtpServerPort(String.valueOf(newPort));}

	public static void setUsername(String newUsername){
		username = newUsername;}

	public static void setPassword(String newPassword){
		password = newPassword;}

	public static void usage(){
		System.out.println("Run command for this program : java -jar target/flopbox-1.0-SNAPSHOT.jar [ftp server address] [ftp server port] [flopbox port] [username] [password]");
	}


	// Lets the user define the variables to connect to the FTP server
	public static void SetVariables(){
		Scanner scanner = new Scanner(System.in);

		System.out.print("FTP Server Address :");
		serverAddress = FtpServerUtil.setFtpServerAddress(scanner.nextLine());

		System.out.print("FTP Server Port :");
		serverPort = FtpServerUtil.setFtpServerPort(scanner.nextLine());

		System.out.print("URI port :");
		BASE_URI = UriUtil.setUri(scanner.nextLine());
        
    	System.out.print("Username :");
		username = scanner.nextLine();

    	System.out.print("Password :");
		password = scanner.nextLine();
        
		System.out.println();
		
        scanner.close();
	}


    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     * 	
     * @return Grizzly HTTP server.
     */
    public static HttpServer startServer()
    {
        // create a resource config that scans for JAX-RS resources and providers
        // in flopbox package
        final ResourceConfig rc = new ResourceConfig().packages("flopbox");

        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }

    
    // Waits for the user next word
    public static void waitWord(){
    	Scanner scanner = new Scanner(System.in);
    	String wordToGet = "stop";
    	String wordGot;
    	do{
    		wordGot = scanner.nextLine().toLowerCase();
			// EXECUTE COMMAND
			}
    	while(!wordGot.equals(wordToGet));
    	
    	scanner.close();
    }


    /**
     * Main method.
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException{
    	SetVariables();
    	// Exit the program if a server address isn't declared by the user
    	if(serverAddress.isEmpty()){
    		System.out.println("An address for the FTP server must be entered.");
    		System.exit(1);}
    	
    	// Exit the program if the username entered by the user is wrong
    	AccountsUtil.loadAccounts();
    	if(!AccountsUtil.checkPassword(username, password)){
    		System.out.println("Wrong username or password.");
    		System.exit(1);}


    	// Creates and connects an FTP client
    	client = new FTPClient();
    	client.connect(serverAddress, serverPort);
    	if(!client.login(username, password)){
    		System.out.println("Login failed.");
    		System.exit(1);}



    	// Start the Grizzly server
		// PLANTE POUR CAUSE D'ADRESSE DEJA UTILISEE, JE N'ARRIVE PAS A TROUVER L'ORIGINE DU PROBLEME
		final HttpServer server = startServer();
		// Wait for user's commands, once he quits, shut the server down
        waitWord();
        server.shutdownNow();
    }
}


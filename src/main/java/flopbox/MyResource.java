package flopbox;

import flopbox.accounts.AccountsUtil;
import flopbox.commands.*;
import java.net.ServerSocket;
import org.apache.commons.net.ftp.FTPClient;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("myresource")
public class MyResource
{
	private FTPClient client = Main.getClient();
	private ServerSocket activeConnection;

	// GET COMMANDS USABLE BY THE SERVER //

	// Log the user in
	@GET
	@Path("login:{username}:{password}")
	@Produces(MediaType.TEXT_PLAIN)
	public String login(FTPClient client, @PathParam("username") String username, @PathParam("password") String password){
		if(AccountsUtil.getAccounts().containsKey(username)){
			if(AccountsUtil.checkPassword(username, password)){
				return LOGIN.execute(client, username, password);}}
		return "Login failed.";}

	// Changes the current directory by the one sent via parameter
	@GET
	@Path("cwd:{pathname}")
	@Produces(MediaType.TEXT_PLAIN)
	public String cwd(FTPClient client, @PathParam("pathname") String pathname){
		return CWD.execute(client, pathname);}

	// Go to parent directory
	@GET
	@Path("cdup")
    @Produces(MediaType.TEXT_PLAIN)
    public String cdup(FTPClient client){
		return CDUP.execute(client);}
	
    // Displays the content of the current directory
	@GET
	@Path("list")
    @Produces(MediaType.TEXT_PLAIN)
    public String list(FTPClient client)
    {
		return LIST.execute(client);
    }

	// Print the current directory
	@GET
	@Path("pwd")
	@Produces(MediaType.TEXT_PLAIN)
	public String pwd(FTPClient client){
		return PWD.execute(client);}

    // Create a directory at the specified path
	@GET
	@Path("mkd:{pathname}")
    @Produces(MediaType.TEXT_PLAIN)
    public String mkd(FTPClient client, @PathParam("pathname") String pathname){
		return MKD.execute(client, pathname);}

	// Retrieve a file
	@GET
	@Path("retr:{pathname}")
    @Produces(MediaType.TEXT_PLAIN)
	public String retr(FTPClient client, @PathParam("pathname") String pathname){
		return RETR.execute(client, pathname);}

	// Upload a file
	@GET
	@Path("stor:{pathname}")
	@Produces(MediaType.TEXT_PLAIN)
	public String stor(FTPClient client, @PathParam("pathname") String pathname){
		return STOR.execute(client, pathname);}
	
	// Rename a specified file
	@GET
	@Path("rn:{oldFilename}:{newFilename}")
    @Produces(MediaType.TEXT_PLAIN)
    public String rn(FTPClient client, @PathParam("oldFilename") String oldFilename, @PathParam("newFilename") String newFilename) {
		return RN.execute(client, oldFilename, newFilename);}

	// Deletes the file specified via para;eter
	@GET
	@Path("dele:{pathname}")
	@Produces(MediaType.TEXT_PLAIN)
	public String dele(FTPClient client, @PathParam("pathname") String pathname){
		return DELE.execute(client, pathname);}

	// Changes the type
	@GET
	@Path("type:{type}")
    @Produces(MediaType.TEXT_PLAIN)
	public String type(FTPClient client, @PathParam("type") int type){
		return TYPE.execute(client, type);}

	// Enables passive mode
	@GET
	@Path("pasv")
	@Produces(MediaType.TEXT_PLAIN)
	public String pasv(FTPClient client){
		return PASV.execute(client);}

	// Enables active mode
	@GET
	@Path("port")
	@Produces(MediaType.TEXT_PLAIN)
	public String port(FTPClient client){
		return PORT.execute(client, activeConnection);}

	// Closes the connection to the server
	@GET
	@Path("quit")
	@Produces(MediaType.TEXT_PLAIN)
	public String quit(){
		return QUIT.execute(client);}
}

package flopbox;

public class UriUtil{
	public static final int BASE_PORT = 2121;
	// Checks the validity of a port
	public static boolean PortVerif(int port){
		return (Math.pow(2, 10) > port || port >= Math.pow(2, 16));}

	// Sets up the URI port specified by the user or the base port if the one specified by the user is wrong
	public static String setUri(String arg){
		int port;
		try{
			port = Integer.parseInt(arg);}
		catch(NumberFormatException e){
			port = BASE_PORT;}
		if(!PortVerif(port)){
			port = BASE_PORT;}
		return "http://localhost:" + port + "/myapp/";}
}

package flopbox.accounts;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class AccountsUtil{
	private static Map<String, String> accounts = new HashMap<String, String>();
	private static File accountsFile = new File("accounts.txt");

	// Returns the different qccounts
	public static Map<String, String> getAccounts()
	{
		return accounts;
	}
	public static void loadAccounts() {
		try{
			String line;
			BufferedReader reader = new BufferedReader(new FileReader(accountsFile));
			while((line = reader.readLine()) != null){
				int cutPosition = line.indexOf(' ');
				String username  = line.substring(0, cutPosition);
				// If the username already exists, we ignore it
				if(accounts.containsKey(username)){
					continue;}
				String password = line.substring(cutPosition+1);
				accounts.put(username, password);}
			reader.close();}
		catch(IOException e){}
	}

	// Checks the password for a given username
	public static boolean checkPassword(String username, String password){
		if(accounts.containsKey(username)){
			return password.equals(accounts.get(username));}
		return username.equals("anonymous");
	}
}
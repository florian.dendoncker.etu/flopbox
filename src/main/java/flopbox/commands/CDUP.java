package flopbox.commands;

import java.io.IOException;
import org.apache.commons.net.ftp.FTPClient;

public class CDUP {
    public static String execute(FTPClient client) {
		String response;
		try {
			if(client.changeToParentDirectory()){
				response = "Went up a directory.";}
			else{
				response = "Couldn't change directory.";}}
		catch(IOException e){
			response = "CDUP Failed.";}
        return response;
    }
}

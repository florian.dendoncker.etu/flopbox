package flopbox.commands;

import java.io.IOException;
import org.apache.commons.net.ftp.FTPClient;

public class CWD {
    public static String execute(FTPClient client, String pathname){
		String response;
		try {
			if(client.changeWorkingDirectory(pathname)){
				response = "The working directory is : " + pathname;}
			else{
				response = "The working directory could not be changed.";}}
		catch(IOException e){
			response = "CWD failed.";}
        return response;
    }
}

package flopbox.commands;

import java.io.IOException;
import org.apache.commons.net.ftp.FTPClient;

public class DELE {
    public static String execute(FTPClient client, String pathname){
		String response;
		try {
			if(client.deleteFile(pathname)){
				response = "File \"" + pathname + "\" deleted.";}
			else{
				response = "Failed to delete \"" + pathname + "\".";}}
		catch(IOException e){
			response = "DELE failed.";}
        return response;
    }
}

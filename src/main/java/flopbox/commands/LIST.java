package flopbox.commands;

import java.io.IOException;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

public class LIST {
    public static String execute(FTPClient client){
		String response = "";
		try{
			FTPFile[] files = client.listFiles();
			for(FTPFile file : files){
				response += file.getName() + "\n";}}
		catch(IOException e){
			response = "LIST failed.";}
        return response;
    }
}

package flopbox.commands;

import java.io.IOException;
import org.apache.commons.net.ftp.FTPClient;

public class LOGIN {
    public static String execute(FTPClient client, String username, String password){
		String response;
		try{
			if(client.login(username, password)){
				response = "The login has succeed.";}
			else{
				response = "The login has not succeed.";}}
		catch(IOException e){
			response = "LOGIN failed.";}
        return response;
    }
}

package flopbox.commands;

import java.io.IOException;
import org.apache.commons.net.ftp.FTPClient;

public class MKD {
    public static String execute(FTPClient client, String pathname){
		String response;
		try{
			if(client.makeDirectory(pathname)){
				response = "Directory \"" + pathname + "\" created.";}
			else{
				response = "Failed to create directory \"" + pathname + "\".";}}
		catch(IOException e){
			response = "MKD failed.";}
        return response;
    }
}

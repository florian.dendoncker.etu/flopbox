package flopbox.commands;

import org.apache.commons.net.ftp.FTPClient;

public class PASV{
    public static String execute(FTPClient client){
		String response;
		if(client.isConnected()){
			client.enterLocalPassiveMode();
			response = "Entering passive mode";}
		else{
			response = "Failed ot enter passive mode.";}
        return response;
    }
}

package flopbox.commands;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import flopbox.Main;

public class PORT {
    public static String execute(FTPClient client, ServerSocket activeConnection){
		String response;
		try {
			if(client.isConnected()){
				activeConnection = new ServerSocket(0);
				int commandResponse = client.port(InetAddress.getByName(Main.getServerAddress()), activeConnection.getLocalPort());
				if(FTPReply.isPositiveCompletion(commandResponse))
					response = "Entering active mode.";
				else
					response = "Failed ot enter active mode.";}
			else{
				response = "Failed ot enter active mode.";}}
		catch(IOException e){
			response = "PORT failed.";}
        return response;
    }
}

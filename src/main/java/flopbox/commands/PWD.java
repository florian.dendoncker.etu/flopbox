package flopbox.commands;

import java.io.IOException;
import org.apache.commons.net.ftp.FTPClient;

public class PWD {
    public static String execute(FTPClient client){
		String response;
        try {
			response = client.printWorkingDirectory();
			if(response != null){
				response = "Current directory : " + response;}
			else{
				response = "can't print current directory.";}}
        catch(IOException e){
        	response = "PWD failed.";}
        return response;
    }
}

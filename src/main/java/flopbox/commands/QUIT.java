package flopbox.commands;

import java.io.IOException;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

public class QUIT{
    public static String execute(FTPClient client){
		String response;
		try{
			int commandResponse = client.quit();
			if(FTPReply.isPositiveCompletion(commandResponse)){
				response = "Successfully exited.";}
			else{
				response = "Could not exit.";}}
		catch(IOException e){
			response = "QUIT failed.";}
        return response;
    }
}

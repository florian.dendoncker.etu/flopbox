package flopbox.commands;

import java.io.IOException;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

public class RETR{
	public static String execute(FTPClient client, String pathname){
		String response;
		try{
			int commandResponse = client.retr(pathname);
			if(FTPReply.isPositiveCompletion(commandResponse)){
				response = "File Retrieved.";}
			else{
				response = "Could not retrieve the file.";}}
		catch(IOException e){
			response = "RETR failed.";}
		return response;
	}
}

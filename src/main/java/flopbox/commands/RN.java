package flopbox.commands;

import java.io.IOException;
import org.apache.commons.net.ftp.FTPClient;

public class RN {
    public static String execute(FTPClient client, String oldFilename, String newFilename){
		String response;
		try{
			if(client.rename(oldFilename, newFilename)){
				response = "Dile \"" + oldFilename + "\" renamed as \"" + newFilename + "\".";}
			else{
				response = "Could not rename the file \"" + oldFilename + "\"."; }}
		catch(IOException e){
			response = "RN failed.";}
        return response;
    }
}

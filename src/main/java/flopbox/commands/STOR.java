package flopbox.commands;

import java.io.IOException;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

public class STOR {
	public static String execute(FTPClient client, String pathname){
		String response;
		try{
			int commandResponse = client.stor(pathname);
			if(FTPReply.isPositiveCompletion(commandResponse)){
				response = "File stored.";}
			else{
				response = "Could not store the file.";}}
		catch(IOException e){
			response = "STOR failed.";}
		return response;
	}
}

package flopbox.commands;

import java.io.IOException;
import org.apache.commons.net.ftp.FTPClient;

public class TYPE {
	public static String execute(FTPClient client, int type) {
		String response;
		try {
			if(client.setFileType(type)){
				response = "The type has been set.";}
			else{
				response = "The type could not be set.";}}
		catch(IOException e) {
			response = "TYPE failed.";}
		return response;
	}
}

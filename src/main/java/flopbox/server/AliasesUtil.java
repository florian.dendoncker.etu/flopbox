package flopbox.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class AliasesUtil {
	private static File aliasesFile = new File("aliases.txt");
	private static Map<String, String> aliases = new HashMap<String, String>();
	public static Map<String, String> getAliases(){
		return aliases;}

	public static void loadAliases(){
		try{
			BufferedReader reader = new BufferedReader(new FileReader(aliasesFile));
			String line;
			while((line = reader.readLine()) != null){
				int cut = line.indexOf(' ');
				String alias  = line.substring(0, cut);
				// If the alias already exists, we skip this part
				if(aliases.containsKey(alias)){
					continue;}
				String server = line.substring(cut+1);
				aliases.put(alias, server);}
			reader.close();}
		catch(IOException e){}
	}
}

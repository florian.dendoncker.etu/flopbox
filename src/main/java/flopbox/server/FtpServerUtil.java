package flopbox.server;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class FtpServerUtil {
	public static final int BASE_PORT = 8080;
	// heck if an adress is correct
	public static boolean checkAdress(String ftpServer){
		try{
			InetAddress.getByName(ftpServer);}
		catch(UnknownHostException e){
			return false;}
		return true;
	}
	

	public static String setFtpServerAddress(String arg) {
		String address = arg;
		if(!checkAdress(address)){
			AliasesUtil.loadAliases();
			if(AliasesUtil.getAliases().containsKey(address)){
				address = AliasesUtil.getAliases().get(address);
				if(!checkAdress(address)){
					address = "";}}
			else{
				address = "";}}
		return address;
	}


	// Check is the FTP port entered is valid
	public static boolean checkFTPPort(int port) {
		return(Math.pow(2, 0) > port || port >= Math.pow(2, 16));}
	

	public static int setFtpServerPort(String arg) {
		int port;
		try{
			port = Integer.parseInt(arg);}
		catch(NumberFormatException e){
			port = BASE_PORT;}
		if(!checkFTPPort(port)){
			port = BASE_PORT;}
		return port;
	}
}
